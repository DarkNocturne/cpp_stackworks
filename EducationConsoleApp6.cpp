#include <iostream>
#include <iomanip>

template <typename T>
class Stack
{
private:
    T* BaseStackObj;
    int size;
    T top;
public:
    Stack(int = 3);
    ~Stack();
    bool push(const T);
    bool pop();
    void printStack();
};

int main()
{
    int SizeOfStack = 0;
    int DeleteValue = 0;

    std::cout << "Type the size of new Stack: ";
    std::cin >> SizeOfStack;
    
    Stack <int> TypicalStack(SizeOfStack);

    int ct = 0;
    while (ct++ != SizeOfStack)
    {
        std::cout << "Type something to put it into Stack of " << SizeOfStack << " elements (pos " << ct << ") : ";
        int temp;
        std::cin >> temp;
        TypicalStack.push(temp);
    }

    TypicalStack.printStack();

    while (DeleteValue == 0 | DeleteValue > SizeOfStack) {
        std::cout << "Type the number of Stack elements to remove (less or equal of " << SizeOfStack << "): ";
        std::cin >> DeleteValue;
    }

    std::cout << "\nDeleting " << DeleteValue << " elements from Stack: \n";

    for (int i = 0; i < DeleteValue; i++) {
        TypicalStack.pop();
    }
    TypicalStack.printStack();

    return 0;
}

template <typename T>
Stack<T>::Stack(int s)
{
    size = s > 0 ? s : 10;
    BaseStackObj = new T[size];
    top = -1;
}

template <typename T>
Stack<T>::~Stack()
{
    delete[] BaseStackObj;
}

template <typename T>
bool Stack<T>::push(const T value)
{
    if (top == size - 1)
        return false;

    top++;
    BaseStackObj[top] = value;

    return true;
}

template <typename T>
bool Stack<T>::pop()
{
    if (top == -1)
        return false;

    BaseStackObj[top] = 0;
    top--;

    return true;
}

template <typename T>
void Stack<T>::printStack()
{
    for (int ix = size - 1; ix >= 0; ix--)
        std::cout << "|" << std::setw(4) << BaseStackObj[ix] << std::endl;
}